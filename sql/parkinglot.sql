-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2012 at 09:20 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `parkinglot`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblcurrentstatus`
--

CREATE TABLE IF NOT EXISTS `tblcurrentstatus` (
  `statusUpdateTimeStamp` datetime NOT NULL,
  `statusPkgLotID` int(11) NOT NULL,
  `statusPkgLotOpen` int(11) NOT NULL,
  `statusPkgLotOccupancy` int(11) DEFAULT NULL,
  PRIMARY KEY (`statusUpdateTimeStamp`,`statusPkgLotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcurrentstatus`
--

INSERT INTO `tblcurrentstatus` (`statusUpdateTimeStamp`, `statusPkgLotID`, `statusPkgLotOpen`, `statusPkgLotOccupancy`) VALUES
('2012-10-05 00:00:00', 1, 1, 25),
('2012-10-06 00:00:00', 2, 1, 25),
('2012-10-07 00:00:00', 3, 0, 25),
('2012-11-07 00:00:00', 4, 1, 25);

-- --------------------------------------------------------

--
-- Table structure for table `tblparkinglots`
--

CREATE TABLE IF NOT EXISTS `tblparkinglots` (
  `PkgLotID` int(11) NOT NULL AUTO_INCREMENT,
  `PkgLotName` varchar(65) NOT NULL,
  `PkgLotCapacity` int(11) NOT NULL,
  PRIMARY KEY (`PkgLotID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tblparkinglots`
--

INSERT INTO `tblparkinglots` (`PkgLotID`, `PkgLotName`, `PkgLotCapacity`) VALUES
(1, 'lot1', 50),
(2, 'lot2', 50),
(3, 'lot3', 50),
(4, 'lot4', 50);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
