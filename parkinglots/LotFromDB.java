package parkinglots;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class LotFromDB {
	
	public static ArrayList processQueryResults(){
		// Get Database Results
		ResultSet res = queryCurrentStatus();
		
		//Initialize lots array
		ArrayList lots = new ArrayList();
		
		try {
			//Iterate through results
			while( res.next() ){
				
				//Initialize Parking Lot Variables
				int id = res.getInt("statusPkgLotID");
				int occupancy = res.getInt("statusPkgLotOccupancy");
				Boolean status = res.getBoolean("statusPkgLotOpen");
				String name = res.getString("PkgLotName");
				Date updated = res.getDate("statusUpdateTimeStamp");
				String description = "";
				
				//Construct Parking Lot
				Lot lot = new Lot( 
						      id
							, occupancy 
							, status
							, name
							, updated
							, description 
						);
				
				//Append lot object to array
				lots.add(lot);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lots;
	}
	
	public static String getParkingLotXML(){
		
		ArrayList lots = processQueryResults();
		
		String root_element = "<campus count='"+lots.size()+"'>\n";
		
		String xml = "<?xml version='1.0' ?>\n"+root_element;
		
		Iterator lot_iterator = lots.iterator();

		while( lot_iterator.hasNext() ){
			xml += ((Lot) lot_iterator.next()).toXML();
			xml += "\n";
		}
		
		String close_xml = "</campus>";
		
		xml += close_xml;
		
		return xml;
	}
	
	public static String getParkingLotJSON(){
		ArrayList lots = processQueryResults();
		
		String root_element = "{'campus':{\n";
		String count = "'count':"+lots.size()+",\n";
		String root_lots = "'lots':{\n";
		
		String json = root_element + count + root_lots;
		
		Iterator lot_iterator = lots.iterator();

		while( lot_iterator.hasNext() ){
			json += ((Lot) lot_iterator.next()).toJSON();
			if ( lot_iterator.hasNext() ){
				json += ",";
			}
			json += "\n";
		}
		
		String close_json = "}\n}\n}";
		
		json += close_json;
		
		return json;
	}
	
	public static ResultSet queryCurrentStatus(){
		
		// Create Database Connection Instance
		Connection conn = null;
		
		// Set Database Connection arguments
		String url = "jdbc:mysql://localhost:3306/";
		String dbName = "parkinglot";
		String driver = "com.mysql.jdbc.Driver";
		String username = "root";
		String password = "";
		try {
			  // Attempt to Connect to DB
			  Class.forName(driver).newInstance();
			  conn = DriverManager.getConnection(url+dbName,username, password);
			  
			  // Query the Table for Results
			  String query = "SELECT status.* , lot.PkgLotName, lot.PkgLotCapacity " +
					  		"FROM tblcurrentstatus status " +
					  		"JOIN tblparkinglots lot ON " +
					  		"status.statusPkgLotID = lot.PkgLotID;";
			  Statement st = conn.createStatement();
		      ResultSet res = st.executeQuery(query);
		      
		      // Return Results
		      return res;
		      
		} catch (Exception e) {
			  e.printStackTrace();
			
			  return null;
		}
	}
	
}

