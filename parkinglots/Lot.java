package parkinglots;

import java.util.Date;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class Lot {
	/*
	 * Parking Lot Object
	 *     ID: Unique Identifier
	 *     Occupancy: The Current Amount of Cars in the Lot
	 *     Status: Describes if the Lot is Open or Closed
	 *     Name: A String Identifier of the Lot
	 *     Updated: The Last time the Lot's current status record was updated.
	 */

	//Initiate Parking Lot Fields
	public int id;
	public int occupancy;
	public Boolean status;
	public String description;
	public String name;
	public Date updated;
	
	//Parking Lot Constructor
	public Lot( 
		  int newId
		, int newOccupancy 
		, Boolean newStatus
		, String newName
		, Date newUpdated
		, String newDescription 
	){
		
		id = newId;
		occupancy = newOccupancy;
		status = newStatus;
		name = newName;
		updated = newUpdated;
		description = newDescription;
	}
	
	//Convert Java Object to JSON
	public String toJSON(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		String jsonLot = gson.toJson(this);
		return jsonLot;
	}

	//Convert Java Object to XML
	public String toXML(){
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("lot", Lot.class);
		String xml = xstream.toXML(this);
		
		return xml;
	}
}

