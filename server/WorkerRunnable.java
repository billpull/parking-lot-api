//**********************************************************************************************************
//  Authors: Bill Pullen, Rafael Borges, Rousseau Policard, 
//           Taoufik Kml, Anthony Traniello  
//  IT 460 API Porject
//  Credit: Used tutorial @ http://tutorials.jenkov.com/java-multithreaded-servers/multithreaded-server.html
//  Server side  app & API for IT460
//**********************************************************************************************************
package server;

import java.io.IOException;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.*;
import java.io.*; 
import java.net.*;

import parkinglots.*;

public class WorkerRunnable implements Runnable{

    protected Socket clientSocket = null;
    protected String serverText   = null;

    public WorkerRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
    }
    
    public void run() {
        try {
            OutputStream output = clientSocket.getOutputStream();
            long time = System.currentTimeMillis();
            
            //Retrieve Input Buffer Stream Request
            BufferedReader in = new BufferedReader( 
            		new InputStreamReader(clientSocket.getInputStream(), "8859_1"));
            
            String request = in.readLine();
            
            String response = Response.respond(request);
            
            output.write((response).getBytes());
               
            output.close();
            in.close();
            System.out.println("Request processed: " + time);
        } catch (IOException e) {
            //report exceptions
            e.printStackTrace();
        }
    }
}

