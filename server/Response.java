package server;

import parkinglots.LotFromDB;

public class Response {

	public static String respond(String request){
		
		String response = "";
		//Server gets Favicon Request so skip that and goto args
        if ( !"GET /favicon.ico HTTP/1.1".equals(request) ){
        	
        	String format = "", apikey ="";
        	
        	// read request string and break down into params array
        	String request_location = request.split(" ")[1];
        	String request_args = request_location.replace("/","");
        	request_args = request_args.replace("?","");
        	String[] queries = request_args.split("&");
        	
        	
        	/* loop through url params and check if they are useful
        	 *      Format: determines JSON or XML
        	 *      Apikey: used to authenticate connection to data
        	 */
        	for ( int i = 0; i < queries.length; i++ ){
        		if( queries[i].split("=")[0].equals("format") ){
        			format = queries[i].split("=")[1];
        		}
        		else if( queries[i].split("=")[0].equals("apikey") ){
        			apikey = queries[i].split("=")[1];
        		}
        	}
        	
        	// If no url params specify defaults.
        	if( apikey.equals("") ){
        		apikey = "None";
        	}
        	
        	if( format.equals("") || !"json".equals(format) ){
        		format = "xml";
        	}
            
        	// Check Api call authentication
            Boolean auth = Auth.authAPIKey(apikey);
            
            if ( auth ){
            	if ( format.equals("xml")){
            		// Retrieve XML Document
            		String xml = LotFromDB.getParkingLotXML();
            		response = xml;
            	}else{
            		//Retrieve JSON
            		String json = LotFromDB.getParkingLotJSON();
            		response = json;
            	}
            }else{
            	response = "Access Denied - User is Not Authenticated";
            }
        }else{
        	response = "Access Denied";
        }
        
        return response;
	}
}
