//**********************************************************************************************************
//  Author: Rafael Borges IT 485
//  Credit: Used tutorial @ http://tutorials.jenkov.com/java-multithreaded-servers/multithreaded-server.html
//  Server app for IT485 Capstone Project
//**********************************************************************************************************
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultiThreadedServer implements Runnable{
// Start server for X seconds     
    public static void main(String[] args) {
    MultiThreadedServer server = new MultiThreadedServer(8080);
    new Thread(server).start();
// Time which server will run for in milliseconds
    try {
         Thread.sleep(20 * 10000);
        } catch (InterruptedException e) {
        e.printStackTrace();
        }
System.out.println("Stopping Server");
server.stop();
}

    protected int          serverPort   = 8080;
    protected ServerSocket serverSocket = null;
    protected boolean      isStopped    = false;
    protected Thread       runningThread= null;

    public MultiThreadedServer(int port){
        this.serverPort = port;
    }

    @Override
    
    //TODO: Make sure you close sockets after connection
    public void run(){
        synchronized(this){
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while(! isStopped()){
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if(isStopped()) {
                    System.out.println("Server Stopped.") ;
                    return;
                }
                throw new RuntimeException(
                    "Error accepting client connection", e);
            }
            new Thread(
                new WorkerRunnable(
                    clientSocket, "Multithreaded Server")
            ).start();
        }
        System.out.println("Server Stopped.") ;
    }


    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop(){
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port 8080", e);
        }
    }

}
